use rs_demo;
# Version 2

# User Table
INSERT INTO user (username, password, enabled) VALUES ("email1@companyOne.com", MD5("password1"), TRUE);
INSERT INTO user (username, password, enabled) VALUES ("email2@companyTwo.com", MD5("password2"), TRUE);
INSERT INTO user (username, password, enabled) VALUES ("email3@companyThree.com", MD5("password3"), TRUE);
INSERT INTO user (username, password, enabled) VALUES ("email4@companyFour.com", MD5("password4"), TRUE);

# User_Info Table
INSERT INTO user_info (user_fk, name, surname, cell_no) VALUES (1, "NameTest1", "SurnameTest1", "071-000-0001");
INSERT INTO user_info (user_fk, name, surname, cell_no) VALUES (2, "NameTest2", "SurnameTest2", "071-000-0002");
INSERT INTO user_info (user_fk, name, surname, cell_no) VALUES (3, "NameTest3", "SurnameTest3", "071-000-0003");
INSERT INTO user_info (user_fk, name, surname, cell_no) VALUES (4, "NameTest4", "SurnameTest4", "071-000-0004");

#User Authorities
INSERT INTO rs_demo.authorities (username, authority) VALUES ('email1@companyOne.com', 'BACKEND');
INSERT INTO rs_demo.authorities (username, authority) VALUES ('email1@companyOne.com', 'FRONTEND');
INSERT INTO rs_demo.authorities (username, authority) VALUES ('email1@companyOne.com', 'RAWDBEND');
INSERT INTO rs_demo.authorities (username, authority) VALUES ('email2@companyTwo.com', 'FRONTEND');
INSERT INTO rs_demo.authorities (username, authority) VALUES ('email2@companyTwo.com', 'BACKEND');
INSERT INTO rs_demo.authorities (username, authority) VALUES ('email3@companyThree.com', 'FRONTEND');

# Groups
INSERT INTO groups (group_name) VALUES ("ADMIN");
INSERT INTO groups (group_name) VALUES ("STAFF");
INSERT INTO groups (group_name) VALUES ("USER");

# Group Authorities
INSERT INTO group_authorities (group_id, authority) VALUES (1, "BACKEND");
INSERT INTO group_authorities (group_id, authority) VALUES (1, "FRONTEND");
INSERT INTO group_authorities (group_id, authority) VALUES (1, "RAWDBEND");
INSERT INTO group_authorities (group_id, authority) VALUES (2, "FRONTEND");
INSERT INTO group_authorities (group_id, authority) VALUES (2, "BACKEND");
INSERT INTO group_authorities (group_id, authority) VALUES (3, "FRONTEND");

# Group Members
INSERT INTO group_members (username, group_id) VALUES ("email1@companyOne.com", 1);
INSERT INTO group_members (username, group_id) VALUES ("email2@companyTwo.com", 2);
INSERT INTO group_members (username, group_id) VALUES ("email3@companyThree.com", 3);
