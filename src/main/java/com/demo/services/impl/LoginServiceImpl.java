package com.demo.services.impl;

import com.demo.dao.LoginDAO;
import com.demo.model.GroupAuthorities;
import com.demo.model.GroupMembers;
import com.demo.model.User;
import com.demo.services.LoginService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Project: RestfulDemo.
 * Created: s34n.
 * Date: 29-02-2016
 * Time: 16:47
 */
public class LoginServiceImpl implements LoginService {

    @Autowired
    LoginDAO loginDAO;

    @Override
    public Boolean isUserLogin(User user) throws Exception {
        return loginDAO.isUserLogin(user);
    }

    @Override
    public User loadUserByUsername(String username) throws Exception {
        return loginDAO.findByUsername(username);
    }

    /**
     * This methods finds the roles in which the group belongs to.
     * E.g. Staff_Group can be assigned ROLE_ADMIN or ROLE_USER access levels.
     *
     * @param groupMembers A list of identified groups.
     * @return A list of Authorities found
     * @throws Exception
     */
    @Override
    public List<GroupAuthorities> findGroupAuthorities(List<GroupMembers> groupMembers) throws Exception {
        return loginDAO.findGroupAuthorities(groupMembers);
    }

    /**
     * RBA (Role Based Authentication). This method finds the group/s in which the user belongs to.
     * Each group is assigned different roles.
     *
     * @param username The username of the authenticated user.
     * @return A list of groups/roles in which the user belongs to.
     * @throws Exception
     */
    @Override
    public List<GroupMembers> findGroupMembers(String username) throws Exception {
        return loginDAO.findGroupMembers(username);
    }
}
