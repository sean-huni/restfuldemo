package com.demo.services;

import com.demo.model.GroupAuthorities;
import com.demo.model.GroupMembers;
import com.demo.model.User;

import java.util.List;

/**
 * Project: RestfulDemo.
 * Created: s34n.
 * Date: 29-02-2016
 * Time: 16:46
 */

/**
 * PROJECT   :RestfulDemo
 * PACKAGE   :com.demo.services
 * USER      :s34n
 * DATE      :29-02-2016
 * TIME      :16:46
 */
public interface LoginService {
   Boolean isUserLogin(User user) throws Exception;
   User loadUserByUsername(String username) throws Exception;

   /**
    * This methods finds the roles in which the group belongs to.
    * E.g. Staff_Group can be assigned ROLE_ADMIN or ROLE_USER access levels.
    *
    * @param groupMembers A list of identified groups.
    * @return A list of Authorities found
    * @throws Exception
    */
   List<GroupAuthorities> findGroupAuthorities(List<GroupMembers> groupMembers) throws Exception;

   /**
    * RBA (Role Based Authentication). This method finds the group/s in which the user belongs to.
    * Each group is assigned different roles.
    *
    * @param username The username of the authenticated user.
    * @return A list of groups/roles in which the user belongs to.
    * @throws Exception
    */
   List<GroupMembers> findGroupMembers(String username) throws Exception;
}
