package com.demo.filter;

import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class HitCounterFilter extends GenericFilterBean {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        String path = request.getRequestURI().substring(request.getContextPath().length());

        if (path.startsWith("/RestfulDemo/views/")) {
            // don't log requests for resources like JS and CSS files
            filterChain.doFilter(servletRequest, servletResponse);
        } else {

            // run normal page execution stuff first, via rest of the chain
            filterChain.doFilter(servletRequest, servletResponse);

            // perform logging. if making use of external logging service, this could be forked off into another thread
            // to allow the http request to return the page to the user while we do logging and stuff
            System.out.println("****");
            System.out.println("URL    : " + request.getRequestURI());
            System.out.println("METHOD : " + request.getMethod());
            System.out.println("QUERY  : " + request.getQueryString());
            System.out.println("SESSION: " + request.getSession().getId());
            System.out.println("TIME   : " + (new SimpleDateFormat("dd-MMM-YYYY HH:mm:ss")).format(new Date(Long.valueOf(System.currentTimeMillis()))));
            System.out.println("****\n");
        }
    }
}
