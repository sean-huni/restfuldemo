package com.demo.controller;

import com.demo.model.GroupAuthorities;
import com.demo.model.GroupMembers;
import com.demo.model.User;
import com.demo.services.LoginService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.NoResultException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Project: RestfulDemo.
 * Created: s34n.
 * Date: 01-03-2016
 * Time: 08:09
 */
@RestController
@RequestMapping(value = "/user/login")
public class LoginCtrl {
    @Autowired
    LoginService loginService;
    private Logger log = Logger.getLogger(LoginCtrl.class.getName());

    @RequestMapping(value = "/findUser", method = RequestMethod.GET)
    public ResponseEntity<User> findAllUsers(
            @NotEmpty @RequestParam("username") String username) {

        User oldUser = new User(), newUser;

        oldUser.setUsername(username);

        try {
            newUser = loginService.loadUserByUsername(oldUser.getUsername());
            if (newUser != null) {
                return new ResponseEntity<User>(newUser, HttpStatus.FOUND);
            }
        } catch (NoResultException ex) {
            return new ResponseEntity<User>(new User(), HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<User>(new User(), HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<User>(new User(), HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/findGroupMembers", method = RequestMethod.GET)
    public ResponseEntity<List<GroupMembers>> findGroupMembers(
            @NotEmpty @RequestParam("username") String username) {
        List<GroupMembers> membersList;
        try {
            membersList = loginService.findGroupMembers(username);
            return new ResponseEntity<List<GroupMembers>>(membersList, HttpStatus.FOUND);
        } catch (Exception e) {
            log.severe(e.getMessage());
            e.printStackTrace();
            List<GroupMembers> groupMembersList = new ArrayList<GroupMembers>();
            return new ResponseEntity<List<GroupMembers>>(groupMembersList, HttpStatus.NOT_FOUND);
        }
    }


    @RequestMapping(value = "/findGroupAuthorities", method = RequestMethod.GET)
    public ResponseEntity<List<GroupAuthorities>> findGroupAuthorities(
            @NotEmpty @RequestParam(value = "members", required = true) String members) {
        List<GroupMembers> groupMembersList = new ArrayList<GroupMembers>();
        List<GroupAuthorities> authoritiesList = new ArrayList<GroupAuthorities>();
        ObjectMapper mapper = new ObjectMapper();

        members = "[" + members + "]";
        members = members.replaceAll("=", ":");
        members = members.replaceAll("[^\\[\\{:\\],}]+", "\"$0\"");
        members = members.replaceAll("\"([\\d+]+)\"", "$1");

        try {
            Integer num = mapper.readValue(members, List.class).size();
            LinkedHashMap<String, Object> newVal = new LinkedHashMap<String, Object>();

            for (int i = 0; i < num; i++) {
                GroupMembers members1 = new GroupMembers();
                newVal = (LinkedHashMap<String, Object>) (mapper.readValue(members, List.class)).iterator().next();
                Integer pk = (Integer) newVal.get("pk");
                Integer groupId = (Integer) newVal.get("groupId");
                String username = (String) newVal.get("username");

                members1.setPk(pk);
                members1.setGroupId(groupId);
                members1.setUsername(username);
                groupMembersList.add(members1);
            }
            //ToDO: remove printout.
            System.out.println("GroupMember's List: " + groupMembersList);

        } catch (IOException e) {
            log.severe(e.getMessage());
            e.printStackTrace();
        }

        try {
            authoritiesList = loginService.findGroupAuthorities(groupMembersList);
            //ToDO: remove printout.
            System.out.println("GroupAuthorities: " + authoritiesList);
            return new ResponseEntity<List<GroupAuthorities>>(authoritiesList, HttpStatus.FOUND);
        } catch (Exception e) {
            log.severe(e.getMessage());
            e.printStackTrace();
//            List<GroupAuthorities> groupAuthoritiesArrayList = new ArrayList<GroupAuthorities>();
            return new ResponseEntity<List<GroupAuthorities>>(authoritiesList, HttpStatus.BAD_REQUEST);
        }

    }
}
