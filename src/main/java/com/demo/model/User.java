package com.demo.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Project: RestfulDemo.
 * Created: s34n.
 * Date: 29-02-2016
 * Time: 16:29
 */
@Entity
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk")
    private Integer pk;
    //    private String name;
//    private String surname;
    @Column(name = "username")
    private String username;
    @Column(name = "password")
    private String password;
    @Column(name = "enabled")
    private Boolean enabled;


    //    @OneToMany(mappedBy = "user")
//    @JoinTable(name = "authorities", joinColumns = {@JoinColumn(name="username")},
//            inverseJoinColumns = {@JoinColumn(name="username")} )
//    @OneToMany
//    @Transient
//    private Set<Authorities> authoritiesHashSet = new HashSet<Authorities>();

    @Transient
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getPk() {
        return pk;
    }

    public void setPk(Integer pk) {
        this.pk = pk;
    }

//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public String getSurname() {
//        return surname;
//    }
//
//    public void setSurname(String surname) {
//        this.surname = surname;
//    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

//    public Set<GroupAuthorities> getGroupAuthorities() {
//        return groupAuthorities;
//    }
//
//    public void setGroupAuthorities(Set<GroupAuthorities> groupAuthorities) {
//        this.groupAuthorities = groupAuthorities;
//    }


//    public Set<Authorities> getGroupAuthorities() {
//        return authoritiesHashSet;
//    }
//
//    public void setGroupAuthorities(Set<Authorities> authoritiesHashSet) {
//        this.authoritiesHashSet = authoritiesHashSet;
//    }

    @Override
    public String toString() {
        return "User [pk:" + pk + ",enabled=" + enabled + ",username=" + username + ",password=" + password + "]";
//     return "User [pk=" + pk + ", name=" + name + ", " +
//                "surname=" + surname + ", username=" + username + ", password=" + password + "]";
    }
}
