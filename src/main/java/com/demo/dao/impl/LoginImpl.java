package com.demo.dao.impl;

import com.demo.dao.LoginDAO;
import com.demo.model.Authorities;
import com.demo.model.GroupAuthorities;
import com.demo.model.GroupMembers;
import com.demo.model.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

/**
 * Project: RestfulDemo.
 * Created: s34n.
 * Date: 29-02-2016
 * Time: 16:25
 */
public class LoginImpl implements LoginDAO {
    @PersistenceContext
    private EntityManager em;

//    @Autowired
//    DataSource dataSource2;

    @Override
    public Boolean isUserLogin(User user) throws Exception {

//        new JdbcTemplate(dataSource2).query("select * from types",
//                new TypeRowMapper());

        String queryString = "select object(o) from User as o";

        String whereStr = " where o.username=:username";
        String andStr = " and o.password=:password";

        queryString += whereStr + andStr;

//        Map<String, Object> resultMap = this.simpleJdbcTemplate.queryForMap(query);

//        User user1 = (User) dataSource2.getConnection().prepareStatement(queryString);


        Query query = em.createQuery(queryString);

        query.setParameter("username", user.getUsername());
        query.setParameter("password", user.getPassword());

        return query.getSingleResult() != null;

    }

    @Override
    public User findByUsername(String username) throws Exception {
        User resultsUser;

        String queryStr, whereStr;

        queryStr = "select object(o) from User as o";
        whereStr = " where o.username=:username";
        queryStr += whereStr;

        Query query = em.createQuery(queryStr);
        query.setParameter("username", username);

        resultsUser = (User) query.getSingleResult();

        if (resultsUser != null) {
            return resultsUser;
        } else{
            return new User();
        }

    }

    @Override
    public List<Authorities> findAuthorities(String username) throws Exception {
        List<Authorities> resultsList;

        String queryStr, whereStr;

        queryStr = "select object(o) from Authorities as o";
        whereStr = " where o.username=:username";
        queryStr += whereStr;

        Query query = em.createQuery(queryStr);
        query.setParameter("username", username);

        resultsList = (List<Authorities>) query.getResultList();

        if (resultsList != null) {
            return resultsList;
        } else{
            resultsList = new ArrayList<Authorities>();
            return resultsList;
        }
    }

    @Override
    public List<GroupAuthorities> findGroupAuthorities(List<GroupMembers> groupMembers) throws Exception {
        List<GroupAuthorities> groupAuthResultsList, finalGroupAuthoritiesList;
        finalGroupAuthoritiesList = new ArrayList<GroupAuthorities>();

        for (GroupMembers groupM : groupMembers){
            String groupAuthQueryStr, groupAuthClauseStr;
            groupAuthQueryStr = "select object(o) from GroupAuthorities as o";
            groupAuthClauseStr = " where o.groupId=:clause1";
            groupAuthQueryStr += groupAuthClauseStr;

            Query query = em.createQuery(groupAuthQueryStr);
            query.setParameter("clause1", groupM.getGroupId());

            groupAuthResultsList = (List<GroupAuthorities>) query.getResultList();
            if(groupAuthResultsList!=null){
                finalGroupAuthoritiesList.addAll(groupAuthResultsList);
            }
        }

        if (finalGroupAuthoritiesList != null) {
//           ToDo: throw new UsernameNotFoundException("Username and Password not found.");
            return finalGroupAuthoritiesList;
        } else{
            finalGroupAuthoritiesList = new ArrayList<GroupAuthorities>();
            return finalGroupAuthoritiesList;
        }
    }

    public List<GroupMembers> findGroupMembers(String username) throws Exception{
        List<GroupMembers> groupMembersResultsList;

        String groupMemQueryStr, groupMemClauseStr;

        groupMemQueryStr = "select object(o) from GroupMembers as o";
        groupMemClauseStr = " where o.username=:username";
        groupMemQueryStr += groupMemClauseStr;

        Query query = em.createQuery(groupMemQueryStr);
        query.setParameter("username", username);

        groupMembersResultsList = (List<GroupMembers>) query.getResultList();

        if (groupMembersResultsList != null) {
            return groupMembersResultsList;
        } else{
            groupMembersResultsList = new ArrayList<GroupMembers>();
            return groupMembersResultsList;
        }
    }
}
