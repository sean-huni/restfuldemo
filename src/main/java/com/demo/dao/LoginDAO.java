package com.demo.dao;

import com.demo.model.Authorities;
import com.demo.model.GroupAuthorities;
import com.demo.model.GroupMembers;
import com.demo.model.User;

import java.util.List;

/**
 * Project: RestfulDemo.
 * Created: s34n.
 * Date: 29-02-2016
 * Time: 16:24
 */
public interface LoginDAO {
    Boolean isUserLogin(User user) throws Exception;
    User findByUsername(String username) throws Exception;
    List<Authorities> findAuthorities(String username) throws Exception;
    List<GroupAuthorities> findGroupAuthorities(List<GroupMembers> groupMembers) throws Exception;
    List<GroupMembers> findGroupMembers(String username) throws Exception;
}
