# INSERT INTO user (name, surname, username, password) VALUES ("NameTest1", "SurnameTest1", "email1@companyOne.com", MD5("password1"));
# INSERT INTO user (name, surname, username, password)VALUES ("NameTest2", "SurnameTest2", "email2@companyTwo.com", MD5("password2"));
# INSERT INTO user (name, surname, username, password)VALUES ("NameTest3", "SurnameTest3", "email3@companyThree.com", MD5("password3"));
# INSERT INTO user (name, surname, username, password)VALUES ("NameTest4", "SurnameTest4", "email4@companyFour.com", MD5("password4"));

# Version 2

# User Table
INSERT INTO user (username, password, enabled) VALUES ("email1@companyOne.com", MD5("password1"), TRUE);
INSERT INTO user (username, password, enabled) VALUES ("email2@companyTwo.com", MD5("password2"), TRUE);
INSERT INTO user (username, password, enabled) VALUES ("email3@companyThree.com", MD5("password3"), TRUE);
INSERT INTO user (username, password, enabled) VALUES ("email4@companyFour.com", MD5("password4"), TRUE);

# User_Info Table
INSERT INTO user_info (user_fk, name, surname, cell_no) VALUES (1, "NameTest1", "SurnameTest1", "071-000-0001");
INSERT INTO user_info (user_fk, name, surname, cell_no) VALUES (2, "NameTest2", "SurnameTest2", "071-000-0002");
INSERT INTO user_info (user_fk, name, surname, cell_no) VALUES (3, "NameTest3", "SurnameTest3", "071-000-0003");
INSERT INTO user_info (user_fk, name, surname, cell_no) VALUES (4, "NameTest4", "SurnameTest4", "071-000-0004");

# Groups
INSERT INTO groups (group_name) VALUES ("ADMIN");
INSERT INTO groups (group_name) VALUES ("STAFF");
INSERT INTO groups (group_name) VALUES ("USER");

# Group Authorities
INSERT INTO group_authorities (group_pk, authority) VALUES (1, "BACKEND");
INSERT INTO group_authorities (group_pk, authority) VALUES (1, "FRONTEND");
INSERT INTO group_authorities (group_pk, authority) VALUES (1, "RAWDBEND");
INSERT INTO group_authorities (group_pk, authority) VALUES (2, "FRONTEND");
INSERT INTO group_authorities (group_pk, authority) VALUES (2, "BACKEND");
INSERT INTO group_authorities (group_pk, authority) VALUES (3, "FRONTEND");

# Group Members
INSERT INTO group_members (username, group_pk) VALUES ("email1@companyOne.com", 1);
INSERT INTO group_members (username, group_pk) VALUES ("email2@companyTwo.com", 2);
INSERT INTO group_members (username, group_pk) VALUES ("email3@companyThree.com", 3);


## Version 3
#
# # Group Helper Class
# INSERT INTO group_helper (group_name) VALUES ("ADMIN");
# INSERT INTO group_helper (group_name) VALUES ("STAFF");
# INSERT INTO group_helper (group_name) VALUES ("USER");
#
# # Group Authorities
# INSERT INTO group_authorities (authority) VALUES ("BACKENDADMIN");
# INSERT INTO group_authorities (authority) VALUES ("FRONTENDADMIN");
# INSERT INTO group_authorities (authority) VALUES ("FRONTENDUSER");
#
# # Groups
# INSERT INTO groups (group_name_fk, group_member_fk, group_authorities_fk) VALUES ("ADMIN", 1, 1);
# INSERT INTO groups (group_name_fk, group_member_fk, group_authorities_fk) VALUES ("STAFF", 2, 2);
# INSERT INTO groups (group_name_fk, group_member_fk, group_authorities_fk) VALUES ("USER", 3, 3);