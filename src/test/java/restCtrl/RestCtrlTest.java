package restCtrl;

import com.demo.model.GroupAuthorities;
import com.demo.model.GroupMembers;
import com.demo.utils.EncryptPass;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import java.util.logging.Logger;

import static org.junit.Assert.assertNotNull;

/**
 * Project: RestfulDemo.
 * Created: s34n.
 * Date: 01-03-2016
 * Time: 09:39
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/xml/applicationContext.xml",
        "classpath:/xml/spring-database.xml",
        "classpath:/xml/test-service.xml"})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RestCtrlTest implements Serializable {
    public static final String REST_SERVICE_URI = "http://localhost:8080/RestfulDemo";
    public static final String username = "email1@companyOne.com";
    public static final String password = new EncryptPass().md5Encrypt("password1");
    private static List<GroupMembers> membersList;
    RestTemplate restTemplate = new RestTemplate();
    String paramEmail, paramPass, q, a;
    private Logger log = Logger.getLogger(RestCtrlTest.class.getName());

    @Before
    public void setup() {
        System.out.println("*****************************Before********************************");
        paramEmail = "username=" + username;
        paramPass = "password=" + password;
        q = "?";
        a = "&";
    }

    /**
     * comment out @Ignore annotation.
     * Only execute restTest when Jetty is up & running from Gradle.
     * Otherwise uncomment @Ignore when jetty is executing.
     * <p>
     * Preconditions: Jetty must be executing the Restful Web-Service.
     * Post-conditions: Uncomment @Ignore annotation.
     */
    @Test
    @Ignore  //Todo: comment-out until jetty has started.
    public void restTest() {
        String obj = restTemplate.getForObject(REST_SERVICE_URI + "/user/login" + q + paramEmail + a + paramPass, String.class);
        System.out.println(obj);
    }

    @Test
    @Ignore //Ignore -> For login
    public void b_test() {
        restTemplate = new RestTemplate();
        List<GroupMembers> groupMembersList = new ArrayList<GroupMembers>();

        groupMembersList = restTemplate.getForObject(REST_SERVICE_URI +
                "/user/login/findGroupMembers" + q + paramEmail, List.class);
        log.info(groupMembersList.toString());
        membersList = groupMembersList;
    }

    @Test
    @Ignore
    public void c_test() {
        restTemplate = new RestTemplate();
//        List<GroupMembers> groupMembers;

        assertNotNull(membersList);
        String paramGroupMembers = "groupMembers=" + membersList;

        List<GroupAuthorities> obj = restTemplate.getForObject(REST_SERVICE_URI +
                "/user/login/findGroupAuthorities" + q + paramGroupMembers, List.class);
        log.info(obj.toString());
    }

    @Test
    @Ignore   //Ignore -> For login
    public void d_test() {
        restTemplate = new RestTemplate();
//        List<GroupMembers> groupMembers;

        assertNotNull(membersList);
//        membersList.addAll(membersList);

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("members", membersList);

        String requestUrl = REST_SERVICE_URI + "/user/login/findGroupAuthorities" + q;

        if (!requestUrl.contains("?")) {
            requestUrl = requestUrl + "?";
        }
        String paramString = "";
        for (String paramName : params.keySet()) {
            paramString = paramString + paramName + "={" + paramName + "}&";
        }
        paramString = paramString.substring(0, paramString.length() - 1);
        requestUrl = requestUrl + paramString;

        if (!params.isEmpty()) {
            for (String key : params.keySet()) {
                Object value = params.get(key);
                if (value instanceof List) {
                    List list = (List) value;
                    String listString = list.toString().replace("[", "").replace("]", "").replace(" ", "");
                    params.put(key, listString);
                }
            }
        }

        log.info(params.toString());
//        if(requestUrl.contains("members={members}")){
//            requestUrl = requestUrl.replace("members={members}","");
//        }
        List<GroupAuthorities> obj = restTemplate.getForObject(requestUrl, List.class, params);
        log.info(obj.toString());

    }

    @Test
    @Ignore
    public void e_test() {
        restTemplate = new RestTemplate();
//        List<GroupMembers> groupMembers;

        assertNotNull(membersList);


        String requestUrl = REST_SERVICE_URI + "/user/login/findGroupAuthorities" + q;

        if (!requestUrl.contains("?")) {
            requestUrl = requestUrl + "?";
        }
        String paramString = "";
        paramString = (membersList.get(0).toString());

        paramString = paramString.substring(0, paramString.length() - 1);
        requestUrl = requestUrl + paramString;

        String listString = null;
        if (membersList instanceof List) {
            List list = (List) membersList;

            listString = list.toString().replace("[", "").replace("]", "").replace(" ", "");
        }

//        log.info(params.toString());
        List<GroupAuthorities> obj = restTemplate.getForObject(requestUrl + listString, List.class);
        log.info(obj.toString());

    }

    @Test
    @Ignore
    public void testing() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String members = "{pk=1,groupId=1,username=email1@companyOne.com},{pk=1,groupId=1,username=email1@companyOne.com}";
        members = "[" + members + "]";
        members = members.replaceAll("=", ":");
        members = members.replaceAll("[^\\[\\{:\\],}]+", "\"$0\"");
//        members = members.replaceAll("[^\\Q\\d\\E]+", "\\d");
        members = members.replaceAll("\"([\\d+]+)\"", "$1");

        List<GroupMembers> groupMembers = new ArrayList<GroupMembers>();
        Integer num = mapper.readValue(members, List.class).size();
        LinkedHashMap<String, Object> newval = new LinkedHashMap<String, Object>();
        for (int i = 0; i < num; i++) {
            GroupMembers members1 = new GroupMembers();
            newval = (LinkedHashMap<String, Object>) (mapper.readValue(members, List.class)).iterator().next();
            Integer pk = (Integer) newval.get("pk");
            Integer groupId = (Integer) newval.get("groupId");
            String username = (String) newval.get("username");

            members1.setPk(pk);
            members1.setGroupId(groupId);
            members1.setUsername(username);
            groupMembers.add(members1);
        }
        groupMembers.toString();
    }
}