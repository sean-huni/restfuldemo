package dao;

import com.demo.dao.LoginDAO;
import com.demo.model.GroupAuthorities;
import com.demo.model.GroupMembers;
import com.demo.model.User;
import com.demo.utils.EncryptPass;
import org.hibernate.cfg.beanvalidation.GroupsPerOperation;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * PROJECT   :RestfulDemo
 * PACKAGE   :dao
 * USER      :s34n
 * DATE      :2016/03/15
 * TIME      :4:49 PM
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/xml/applicationContext.xml",
        "classpath:/xml/security-config.xml",
        "classpath:/xml/spring-database.xml",
        "classpath:/xml/test-service.xml"})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DaoTests {
    @Autowired
    LoginDAO loginDAO;
    private User user = new User();
    private EncryptPass encryptPass = new EncryptPass();

    @Before
    public void setup(){
        System.out.println("*****************************Before********************************");

        user.setUsername("email1@companyOne.com");
        user.setPassword(encryptPass.md5Encrypt("password1"));
    }

    @Test
    public void a_test() throws Exception{
        assertNotNull(user);
        List<GroupAuthorities> authoritiesList = new ArrayList<GroupAuthorities>();
        List<GroupMembers> groupMembersList = new ArrayList<GroupMembers>();

        groupMembersList.addAll(loginDAO.findGroupMembers(user.getUsername()));
        authoritiesList.addAll(loginDAO.findGroupAuthorities(groupMembersList));
        assertNotNull( authoritiesList );
    }
}
