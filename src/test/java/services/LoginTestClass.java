package services;

import com.demo.model.Authorities;
import com.demo.model.GroupAuthorities;
import com.demo.model.GroupMembers;
import com.demo.model.User;
import com.demo.services.LoginService;
//import com.demo.services.impl.LoginDetailsService;
import com.demo.utils.EncryptPass;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;
import java.util.logging.Logger;

import static org.junit.Assert.assertNotNull;

/**
 * Project: RestfulDemo.
 * Created: s34n.
 * Date: 01-03-2016
 * Time: 09:39
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/xml/applicationContext.xml",
        "classpath:/xml/security-config.xml",
        "classpath:/xml/spring-database.xml",
        "classpath:/xml/test-service.xml"})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class LoginTestClass {
    private final static Logger log = Logger.getLogger(LoginTestClass.class.getName());
    @Autowired
    LoginService loginService;
    private User newUser;
    private EncryptPass encryptPass;

//    @Autowired
//    LoginDetailsService loginDetailsService;

    @Before
    public void setup(){
        System.out.println("*****************************Before********************************");
        newUser = new User();
        encryptPass = new EncryptPass();
        newUser.setUsername("email1@companyOne.com");
        newUser.setPassword(encryptPass.md5Encrypt("password1"));
    }

    @Test
    public void a_test(){
//        loginDetailsService.loadUserByUsername(newUser.getUsername());
    }

    @Test
    public void b_test() throws Exception {
        List<GroupMembers> groupList = loginService.findGroupMembers(newUser.getUsername());
        assertNotNull(groupList);
        log.info(groupList.toString());
        List<GroupAuthorities> userAuthorities = loginService.findGroupAuthorities(groupList);
        assertNotNull(userAuthorities);
        log.info(userAuthorities.toString());
    }

    @Test
    public void c_test(){

    }


}