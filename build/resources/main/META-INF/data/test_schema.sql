DROP SCHEMA IF EXISTS rs_demo;
CREATE SCHEMA IF NOT EXISTS rs_demo;

USE rs_demo;

# Version 2

CREATE TABLE user (
  pk       INT          NOT NULL AUTO_INCREMENT PRIMARY KEY,
  username VARCHAR(64)  NOT NULL UNIQUE,
  password VARCHAR(145) NOT NULL,
  enabled  BOOLEAN      NOT NULL,
  UNIQUE INDEX unq_inx_username(username)
);

CREATE TABLE user_info (
  pk      INT         NOT NULL AUTO_INCREMENT PRIMARY KEY,
  user_fk INT         NOT NULL,
  name    VARCHAR(45) NOT NULL,
  surname VARCHAR(45) NOT NULL,
  cell_no VARCHAR(16) NOT NULL,
  CONSTRAINT fk_user FOREIGN KEY (user_fk) REFERENCES user (pk)
);

#   1-1 Relationship. Does not support expansions. Tightly coupled.
CREATE TABLE authorities
(
  pk        INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  username  VARCHAR(60)     NOT NULL,
  authority VARCHAR(35)     NOT NULL,
  unique INDEX inx_authorities(authority, username),
  CONSTRAINT fk_authorities_user FOREIGN KEY (username) REFERENCES user (username)
)
  COMMENT = '1-1 Relationship. Not expansible/extensible';

CREATE TABLE groups (
  id         INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  group_name VARCHAR(64)  NOT NULL UNIQUE
) COMMENT = 'Name of the group that shall have 1-* Authorities';

CREATE TABLE group_authorities (
  id        INT          NOT NULL AUTO_INCREMENT PRIMARY KEY ,
  group_id  INT UNSIGNED NOT NULL,
  authority VARCHAR(64)  NOT NULL,
  unique INDEX inx_group_authorities(authority, group_id),
  CONSTRAINT fk_auth_group_id FOREIGN KEY (group_id) REFERENCES groups (id)
);

CREATE TABLE group_members (
  id       INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  username VARCHAR(64)  NOT NULL,
  group_id INT UNSIGNED NOT NULL,
  unique INDEX inx_group_members(username, group_id),
  CONSTRAINT fk_member_group_id FOREIGN KEY (group_id) REFERENCES groups (id)
);

CREATE TABLE user_token (
  pk         INT                 NOT NULL AUTO_INCREMENT PRIMARY KEY,
  user_fk    INT                 NOT NULL,
  token      VARCHAR(350) UNIQUE NOT NULL,
  expiryDate DATE                NOT NULL,
  created    TIMESTAMP                    DEFAULT CURRENT_TIMESTAMP,
  updated    TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  CONSTRAINT fk_user_token FOREIGN KEY (user_fk) REFERENCES user (pk)
);

CREATE TABLE persistent_logins (
  pk        INT AUTO_INCREMENT PRIMARY KEY,
  username  VARCHAR(64)  NOT NULL,
  series    VARCHAR(64) UNIQUE,
  token     VARCHAR(364) NOT NULL,
  last_used TIMESTAMP    NOT NULL,
  index inx_token_log(token),
  index inx_username_log(username)
);
