DROP SCHEMA IF EXISTS rs_demo;
CREATE SCHEMA IF NOT EXISTS rs_demo;

USE rs_demo;

# CREATE TABLE user(
#   pk INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
#   name varchar(45) NOT NULL,
#   surname varchar(45) NOT NULL,
#   username varchar(100) NOT NULL UNIQUE,
#   password varchar(145) NOT NULL
# );

# Version 2

CREATE TABLE user (
  pk       INT          NOT NULL AUTO_INCREMENT PRIMARY KEY,
  username VARCHAR(64)  NOT NULL UNIQUE,
  password VARCHAR(145) NOT NULL,
  enabled  BOOLEAN      NOT NULL,
  UNIQUE INDEX unq_inx_username(username)
);

CREATE TABLE user_info (
  pk      INT         NOT NULL AUTO_INCREMENT PRIMARY KEY,
  user_fk INT         NOT NULL,
  name    VARCHAR(45) NOT NULL,
  surname VARCHAR(45) NOT NULL,
  cell_no VARCHAR(16) NOT NULL,
  CONSTRAINT fk_user FOREIGN KEY (user_fk) REFERENCES user (pk)
);

#   1-1 Relationship. Does not support expansions. Tightly coupled.
CREATE TABLE authorities
(
  pk        INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  username  VARCHAR(60)     NOT NULL,
  authority VARCHAR(35)     NOT NULL,
  CONSTRAINT fk_authorities_user FOREIGN KEY (username) REFERENCES user (username)
)
  COMMENT = '1-1 Relationship. Not expansible/extensible';

# Loosely coupled & supports extensibility.
CREATE TABLE group_authorities (
  pk        INT         NOT NULL AUTO_INCREMENT PRIMARY KEY,
  authority VARCHAR(50) NOT NULL
  #   CONSTRAINT fk_group_authorities_group FOREIGN KEY (group_pk) REFERENCES groups (pk)
);

CREATE TABLE group_helper (
  pk         INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  group_name VARCHAR(50) UNIQUE,
  INDEX unq_inx_group_name(group_name)
);

CREATE TABLE user_token (
  pk         INT                  NOT NULL AUTO_INCREMENT PRIMARY KEY,
  user_fk int not null,
  token      VARCHAR(350) UNIQUE NOT NULL,
  expiryDate DATE                 NOT NULL,
  created    TIMESTAMP                     DEFAULT CURRENT_TIMESTAMP,
  updated    TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  CONSTRAINT fk_user_token FOREIGN KEY (user_fk) REFERENCES user(pk)
);

# Removed Reason: same as the User Table.
# CREATE TABLE group_members (
#   pk       INT AUTO_INCREMENT PRIMARY KEY,
#   username VARCHAR(64) NOT NULL,
#   group_pk INT         NOT NULL,
#   index uni_inx_username(username)
# #   CONSTRAINT fk_group_members_group FOREIGN KEY (group_pk) REFERENCES groups (pk)
# );

CREATE TABLE groups (
  pk                   INT         NOT NULL AUTO_INCREMENT PRIMARY KEY,
  group_name_fk        VARCHAR(50) NOT NULL,
  group_member_fk      INT         NOT NULL,
  group_authorities_fk INT         NOT NULL,
  INDEX index_member_authorities(group_name_fk, group_member_fk, group_authorities_fk),
  INDEX index_group_name(group_name_fk),
  CONSTRAINT fk_group_name FOREIGN KEY (group_name_fk) REFERENCES group_helper (group_name),
  CONSTRAINT fk_group_member FOREIGN KEY (group_member_fk) REFERENCES user (pk),
  CONSTRAINT fk_group_authorities FOREIGN KEY (group_authorities_fk) REFERENCES group_authorities (pk)
)
  COMMENT = 'Name of the group that shall have 1-* Authorities';

CREATE TABLE persistent_logins (
  pk        INT AUTO_INCREMENT PRIMARY KEY,
  username  VARCHAR(64)  NOT NULL,
  series    VARCHAR(64) UNIQUE,
  token     VARCHAR(364) NOT NULL,
  last_used TIMESTAMP    NOT NULL
);
